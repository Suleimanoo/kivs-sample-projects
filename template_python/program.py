#!/usr/bin/env python3

import argparse

def debug(string):
	print("DEBUG:", string)

def solution(string):
	print("SOLUTION:", string)

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('command', type=str, help='the command to execute')

	args = parser.parse_args()
	debug("Running command:", args.command)
	if args.command == "example":
		# Hier die Programmlogik
		pass
	else:
		debug("y u no supply correct command? (╯°□°)╯︵ ┻━┻")

